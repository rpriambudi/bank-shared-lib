import { Module, DynamicModule, Global } from "@nestjs/common";
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { BaseConsumer } from './abstracts/base.consumer';
import { SagaState } from './entities/saga-state.entity';
import { RabbitBrokerPublisher } from "./implementations/brokers/rabbit-broker-publisher.impl";
import { RabbitBrokerConsumer } from "./implementations/brokers/rabbit-broker-consumer.impl";
import { SagaServiceImpl } from "./implementations/services/saga-service.impl";
import { ModuleMetadata } from "@nestjs/common/interfaces";
import { BaseSaga } from "./sagas/base.saga";
import { RabbitSagaExecutor } from "./implementations/brokers/rabbit-saga-executor.impl";
import { AesEncryptor } from "./implementations/securities/aes-encryptor.impl";
import { AesDecryptor } from "./implementations/securities/aes-decryptor.impl";
import { FirebaseNotificationServiceImpl } from "./implementations/services/firebase-notification-service.impl";

const sharedProviderFactory = (brokerOption: BrokerAsyncOption) => [
    {
        provide: 'BrokerConsumer',
        useFactory: (brokerOption) => {
            return new RabbitBrokerConsumer(brokerOption);
        },
        inject: ['BrokerAsyncOption']
    },
    {
        provide: 'BrokerPublisher',
        useFactory: () => {
            return new RabbitBrokerPublisher();
        },
    },
    {
        provide: 'BrokerAsyncOption',
        useFactory: brokerOption.useFactory,
        inject: brokerOption.inject
    }
]

const sharedSagaProviderFactory = (sagaOption: SagaAsyncOption) => [
    {
        provide: 'SagaService',
        useFactory: (repository) => {
            return new SagaServiceImpl(repository);
        },
        inject: [getRepositoryToken(SagaState)]
    },
    {
        provide: 'BrokerPublisher',
        useFactory: () => {
            return new RabbitBrokerPublisher()
        }
    },
    {
        provide: 'SagaExecutor',
        useFactory: (sagaOption) => {
            return new RabbitSagaExecutor(sagaOption);
        },
        inject: ['SagaAsyncOption'],
    },
    {
        provide: 'SagaAsyncOption',
        useFactory: sagaOption.useFactory,
        inject: sagaOption.inject
    },
    TypeOrmModule
]

const sharedSecurityProviderFactory = () => [
    {
        provide: 'Encryptor',
        useClass: AesEncryptor
    },
    {
        provide: 'Decryptor',
        useClass: AesDecryptor
    }
]

const sharedNotificationProviderFactory = (notificationAsyncOption: NotificationAsyncOption) => [
    {
        provide: 'NotificationService',
        useFactory: (notificationOption: NotificationOption) => {
            return new FirebaseNotificationServiceImpl(notificationOption.firebaseCredential, notificationOption.databaseUrl);
        },
        inject: ['CommonAsyncOption']
    },
    {
        provide: 'CommonAsyncOption',
        useFactory: notificationAsyncOption.useFactory,
        inject: notificationAsyncOption.inject
    }
]

@Global()
@Module({})
export class BankSharedModule {
    static registerSaga(sagaOption: SagaAsyncOption): DynamicModule {
        return {
            module: BankSharedModule,
            imports: [
                TypeOrmModule.forFeature([SagaState])
            ],
            providers: sharedSagaProviderFactory(sagaOption),
            exports: sharedSagaProviderFactory(sagaOption)
        }
    }

    static registerHandler(brokerOption: BrokerAsyncOption): DynamicModule {
        return {
            module: BankSharedModule,
            imports: [],
            providers: sharedProviderFactory(brokerOption),
            exports: sharedProviderFactory(brokerOption)
        }
    }
}

@Global()
@Module({})
export class BankSecurityModule {
    static register(): DynamicModule {
        return {
            module: BankSecurityModule,
            imports: [],
            providers: [
                ...sharedSecurityProviderFactory(),
            ],
            exports: [
                ...sharedSecurityProviderFactory(),
            ]
        }
    }
}

@Global()
@Module({})
export class BankNotificationModule {
    static register(notificationAsyncOption: NotificationAsyncOption): DynamicModule {
        return {
            module: BankNotificationModule,
            imports: [],
            providers: [
                ...sharedNotificationProviderFactory(notificationAsyncOption)
            ],
            exports: [
                ...sharedNotificationProviderFactory(notificationAsyncOption)
            ]
        }
    }
}

export class BrokerOption {
    commandList: BaseConsumer[]
}

export class SagaOption {
    sagaList: BaseSaga[]
}

export class NotificationOption {
    firebaseCredential: string;
    databaseUrl: string;
}

export interface BrokerAsyncOption extends Pick<ModuleMetadata, 'imports'> {
    useFactory?: (...args: any[]) => BrokerOption,
    inject: any[]
}

export interface SagaAsyncOption extends Pick<ModuleMetadata, 'imports'> {
    useFactory?: (...args: any[]) => SagaOption,
    inject: any[]
}

export interface NotificationAsyncOption extends Pick<ModuleMetadata, 'imports'> {
    useFactory?: (...args: any[]) => NotificationOption,
    inject?: any[]
}