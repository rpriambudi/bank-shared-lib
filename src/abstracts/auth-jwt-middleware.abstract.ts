import { NestMiddleware } from "@nestjs/common";
import * as rp from 'request-promise';
import { JWKS, JWT } from 'jose';
import { InvalidTokenException } from './../exceptions/invalid-token.exception';
import { InvalidRoleException } from './../exceptions/invalid-role.exception';
import { TokenNotFoundException } from './../exceptions/token-not-found.exception';

export abstract class AuthJwtMiddleware implements NestMiddleware {
    abstract getGuards(): string[];
    abstract getJwksUrl(): string;
    abstract getClientId(): string;

    async use(req: any, res: any, next: () => void) {
        const guards = this.getGuards();
        req['verifiedObj'] = await this.runJWTStrategy(req, guards);
        next();
    }

    private async runJWTStrategy(request: any, guards: string[]) {
        const authorization = request.headers['authorization'];
        if (!authorization)
            throw new TokenNotFoundException('Token not provided.')
            
        let token = authorization.split(' ');
        token = token[1];

        const verify = await this.keycloakJwksValidation(token);
        const find = this.findRoleInToken(verify, guards);

        if (!find)
            throw new InvalidRoleException('Insufficient access');

        return verify;
    }

    private async keycloakJwksValidation(token: string): Promise<any> {
        const jwksUrl = this.getJwksUrl();
        const result = await rp({
            method: 'GET',
            uri: jwksUrl
        });
        const keystore = JWKS.asKeyStore(JSON.parse(result));
        
        try {
            return JWT.verify(token, keystore);
        } catch (error) {
            throw new InvalidTokenException(error.message);
        }
    }

    private findRoleInToken(tokenObj: any, guards: string[]) {
        const clientId = this.getClientId();
        const clientData = tokenObj['resource_access'][clientId];
        if (!clientData)
            throw new InvalidRoleException('Insufficient access.');

        const roles: [] = tokenObj['resource_access'][clientId]['roles'];
        if (!roles || roles.length === 0)
            throw new InvalidRoleException('Role not found.');

        let find = false;
        let supportedRole = {}
        for (const guard of guards) {
            supportedRole[guard] = true;
        }

        for (const role of roles) {
            if (supportedRole[role]) {
                find = true;
                break;
            }
        }
        
        return find;
    }
}

export class MockAuthJwtMiddleware extends AuthJwtMiddleware {
    getJwksUrl(): string {
        return 'Mock';
    }
    getClientId(): string {
        return 'Mock';
    }
    getGuards(): string[] {
        return [];
    }

    runMockStrategy(req: any, guards: string[]) {
        const mockUserContext: any = {
            name: 'Test Branch',
            email: 'test@mail.co',
            branch_code: '001'
        }
        return mockUserContext;
    }
}