export abstract class BaseConsumer {
    abstract getCommandName(): string;
    abstract handle(data: any): Promise<any>;
}