export abstract class GenericException extends Error {
    constructor(message: string) {
        super(message);
    }

    abstract getErrorCode(): string;
    abstract getDisplayCode(): string;

    public getMessage(): string {
        return this.message;
    }
}