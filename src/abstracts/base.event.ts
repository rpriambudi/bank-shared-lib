import { BaseCommand } from './base.command';

export abstract class BaseEvent {
    constructor(private readonly command: BaseCommand) {}
    abstract getEventName(): string;
    abstract getUpdatedData(stateData: any, eventData: any);

    getEventData(data: any): any {
        return data;
    }

    getCommand(): BaseCommand {
        return this.command;
    }
}