export abstract class BaseCommand {
    abstract getCommandName(): string;

    getCommandData(data: any) {
        return data;
    }
}