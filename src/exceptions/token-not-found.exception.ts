import { GenericException } from './../abstracts/generic-exception.abstract';

export class TokenNotFoundException extends GenericException {
    getDisplayCode(): string {
        return 'TOKEN_NOT_FOUND';
    }

    getErrorCode(): string {
        return '401991';
    }

    constructor(message: string) {
        super(message);
    }
}