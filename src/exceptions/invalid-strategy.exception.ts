import { GenericException } from './../abstracts/generic-exception.abstract';

export class InvalidStrategyException extends GenericException {
    getDisplayCode(): string {
        return 'INVALID_STRATEGY';
    }

    getErrorCode(): string {
        return '400994';
    }

    constructor(message: string) {
        super(message);
    }
}