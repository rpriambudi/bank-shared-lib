import { GenericException } from './../abstracts/generic-exception.abstract';

export class InvalidTokenException extends GenericException {
    getDisplayCode(): string {
        return 'INVALID_TOKEN';
    }

    getErrorCode(): string {
        return '401992';
    }

    constructor(message: string) {
        super(message);
    }
}