import { GenericException } from './../abstracts/generic-exception.abstract';
export class InvalidRoleException extends GenericException {
    constructor(message: string) {
        super(message);
    }

    getDisplayCode(): string {
        return 'INVALID_ROLE';
    }
    getErrorCode(): string {
        return '403993';
    }
}
