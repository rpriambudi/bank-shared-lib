import { Injectable } from '@nestjs/common';
import * as amqp from 'amqplib';
import { IBrokerConsumer } from './../../interfaces/brokers/broker-consumer.interface';
import { BrokerOption } from './../../shared.module';

@Injectable()
export class RabbitBrokerConsumer implements IBrokerConsumer {
    constructor(private readonly brokerOption: BrokerOption) {}

    async consume(): Promise<void> {
        const connection = await this.connectionToBroker();

        let commandName: string;
        for (const command of this.brokerOption.commandList) {
            commandName = command.getCommandName();

            await connection.assertQueue(commandName, { durable: true});
            connection.consume(
                commandName, 
                async (message: any) => {
                    const data = Buffer.from(message.content, 'base64').toString();
                    try {
                        await command.handle(JSON.parse(data));
                        connection.ack(message);
                    } catch (error) {
                        console.log(error);
                    }
                }
            )
        }
    }
    
    private async connectionToBroker() {
        const brokerHost = process.env.BROKER_HOST_URL;
        const connection = await amqp.connect(brokerHost);
        return await connection.createChannel();
    }
}