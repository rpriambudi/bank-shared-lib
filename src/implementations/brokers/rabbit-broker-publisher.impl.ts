import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import * as amqp from 'amqplib';
import { IBrokerPublisher } from './../../interfaces/brokers/broker-publisher.interface';

@Injectable()
export class RabbitBrokerPublisher implements IBrokerPublisher {
    constructor() {}

    async dispatch(queueName: string, data: any): Promise<void> {
        const connection = await this.connectionToBroker(queueName);
        await connection.sendToQueue(queueName, Buffer.from(JSON.stringify(data)));
    }

    private async connectionToBroker(queueName: string) {
        const brokerHost = process.env.BROKER_HOST_URL;
        const connection = await amqp.connect(brokerHost);
        const channel = await connection.createChannel();
        
        await channel.assertQueue(queueName);
        return channel;
    }
}