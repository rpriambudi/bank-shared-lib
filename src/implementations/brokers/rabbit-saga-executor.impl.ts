import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import * as amqp from 'amqplib';
import { IBrokerConsumer } from './../../interfaces/brokers/broker-consumer.interface';
import { SagaOption } from './../../shared.module';
import { BaseEvent } from './../../abstracts/base.event';
import { SagaState } from './../../entities';
import { BaseSaga } from './../../sagas';

@Injectable()
export class RabbitSagaExecutor implements IBrokerConsumer {
    constructor(
        private readonly sagaOptions: SagaOption
    ) {}

    async consume(): Promise<void> {
        const connection = await this.connectionToBroker();

        let events: BaseEvent[];
        for (const saga of this.sagaOptions.sagaList) {
            events = saga.getEvents();

            for (const event of events) {
                await this.consumeEvent(connection, event, saga);
            }
        }
    }

    private async connectionToBroker() {
        const brokerHost = process.env.BROKER_HOST_URL;
        const connection = await amqp.connect(brokerHost);
        return await connection.createChannel();
    }

    private async consumeEvent(connection: any, event: BaseEvent, saga: BaseSaga ): Promise<void> {
        await connection.assertQueue(event.getEventName(), { durable: true});
        connection.consume(
            event.getEventName(), 
            async (message: any) => {
                const data = Buffer.from(message.content, 'base64').toString();
                try {
                    await saga.handle(event, JSON.parse(data));
                    connection.ack(message);
                } catch (error) {
                    console.log(error);
                }
            }
        );
    }
}