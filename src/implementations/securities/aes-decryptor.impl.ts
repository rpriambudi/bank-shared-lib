import * as crypto from 'crypto';
import { Decryptor } from './../../interfaces/securities/decryptor.interface';

export class AesDecryptor implements Decryptor {
    decrypt(encryptedData: string): string {
        const decodeBuffer = Buffer.from(encryptedData, 'base64');
        const decodeObject: {iv: string, data: string} = JSON.parse(decodeBuffer.toString('utf8'));
        const configKey = process.env.HASH_KEY;
        const decipher = crypto.createDecipheriv(
            'aes-256-cbc', 
            Buffer.from(configKey, 'base64'), 
            Buffer.from(decodeObject['iv'], 'base64')
        );

        let decoded = decipher.update(decodeObject.data, 'base64', 'utf8');
        decoded += decipher.final('utf8');

        return decoded;
    }
}