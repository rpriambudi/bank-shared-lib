import * as crypto from 'crypto';
import { Encryptor } from './../../interfaces/securities/encryptor.interface';

export class AesEncryptor implements Encryptor {
    encrypt(data: string): string {
        const iv = crypto.randomBytes(16);
        const configKey = process.env.HASH_KEY;
        const cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(configKey, 'base64'), iv);
        let encoded = cipher.update(data, 'utf8', 'base64');
        encoded += cipher.final('base64');
        
        const encodeJson = {
            iv: iv.toString('base64'),
            data: encoded
        }
        const base64Json = Buffer.from(JSON.stringify(encodeJson));
        return base64Json.toString('base64');
    }
}