import { Repository } from 'typeorm';
import { SagaState } from './../../entities/saga-state.entity';
import { SagaService } from './../../interfaces/services/saga-service.interface';

export class SagaServiceImpl implements SagaService {
    constructor(
        private readonly repository: Repository<SagaState>
    ) {}

    async findSagaByStateId(stateId: number): Promise<SagaState> {
        return await this.repository.findOne({ id: stateId });
    }
}