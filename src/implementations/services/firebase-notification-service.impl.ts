import * as firebase from 'firebase-admin';
import { NotificationService } from './../../interfaces/services/notification-service.interface';

export class FirebaseNotificationServiceImpl implements NotificationService {
    private readonly firebaseApp: firebase.app.App;

    constructor(firebaseCredential: string, databaseUrl: string) {
        const firebaseApp: firebase.app.App = firebase.initializeApp({
            credential: firebase.credential.cert(firebaseCredential),
            databaseURL: databaseUrl
        });
        this.firebaseApp = firebaseApp;
    }

    async dispatch(topicName: string, data: any): Promise<void> {
        const message = {
            data: data,
            topic: topicName
        }

        const response = await this.firebaseApp.messaging().send(message);
        console.log(response);
    }
    
}