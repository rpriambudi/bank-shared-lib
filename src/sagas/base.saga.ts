import { Injectable, Inject } from "@nestjs/common";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { SagaState } from "./../entities";
import { IBrokerPublisher } from "./../interfaces";
import { BaseCommand, BaseEvent } from "./../abstracts";

@Injectable()
export abstract class BaseSaga {
    constructor(
        @InjectRepository(SagaState) private readonly repository: Repository<SagaState>,
        @Inject('BrokerPublisher') private readonly brokerPublisher: IBrokerPublisher
    ) {}

    abstract getEvents(): BaseEvent[];

    async startSaga(data: any, startEvent: string): Promise<SagaState> {
        const saga = this.repository.create();
        saga.state = startEvent;
        saga.data = data;

        await this.repository.save(saga);
        await this.brokerPublisher.dispatch(startEvent, { stateId: saga.id });
        return saga;
    }

    async handle(event: BaseEvent, jsonObject: any): Promise<void> {
        const eventData = event.getEventData(jsonObject);
        const command: BaseCommand = event.getCommand();

        if (command) {
            const saga = await this.repository.findOne(eventData.stateId);
            saga.state = event.getEventName();
            saga.data = event.getUpdatedData(saga.data, eventData);
            
            await this.repository.save(saga);
            await this.publishCommand(saga.id, command);
            return;
        } else {
            if (eventData.error) {
                await this.failSaga(eventData.stateId, event.getEventName(), eventData.error);
                return;
            }
            await this.finishSaga(eventData.stateId);
            return;
        }
    }

    async publishCommand(stateId: number, command: BaseCommand): Promise<void> {
        const state = await this.repository.findOne({ id: stateId });
        const data = command.getCommandData(state.data);
        data.stateId = state.id;

        await this.brokerPublisher.dispatch(command.getCommandName(), data);
    }

    async finishSaga(stateId: number): Promise<void> {
        const saga = await this.repository.findOne(stateId);
        saga.state = 'sagaFinished';

        await this.repository.save(saga);
    }

    async failSaga(stateId: number, eventName: string, errorMessage: string): Promise<void> {
        const saga = await this.repository.findOne(stateId);
        saga.state = eventName;
        saga.description = errorMessage;

        await this.repository.save(saga);
    }
}