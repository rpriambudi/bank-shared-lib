export interface Encryptor {
    encrypt(data: string): string;
}