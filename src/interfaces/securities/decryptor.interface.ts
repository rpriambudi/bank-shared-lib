export interface Decryptor {
    decrypt(encryptedData: string): string;
}