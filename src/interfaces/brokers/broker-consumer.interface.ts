export interface IBrokerConsumer {
    consume(): Promise<void>;
}