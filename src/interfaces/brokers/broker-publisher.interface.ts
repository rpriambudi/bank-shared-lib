export interface IBrokerPublisher {
    dispatch(queueName: string, data: any): void;
}