export interface NotificationService {
    dispatch(topicName: string, data: any): Promise<void>;
}