import { SagaState } from './../../entities/saga-state.entity';

export interface SagaService {
    findSagaByStateId(stateId: number): Promise<SagaState>;
}