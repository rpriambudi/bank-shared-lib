import { ExceptionFilter, ArgumentsHost, Catch } from '@nestjs/common';
import { GenericException } from  './../abstracts/generic-exception.abstract';

@Catch()
export class HttpGenericFilter implements ExceptionFilter {
    private basicExceptionCode = {
        '401991': 401,
        '401992': 401,
        '403993': 403,
        '400994': 400
    };
    constructor(
        private readonly jsonData: any
    ) {}
    catch(exception: any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        if (exception instanceof GenericException) {
            const statusCode = this.jsonData[exception.getErrorCode()];
            const basicCode = this.basicExceptionCode[exception.getErrorCode()];

            if (basicCode) {
                response.status(basicCode).json({errorCode: exception.getDisplayCode(), message: exception.getMessage()});
                return;
            }

            if (statusCode) {
                response.status(statusCode).json({errorCode: exception.getDisplayCode(), message: exception.getMessage()});
                return;
            }

            response.status(500).json({errorCode: 'UNKNOWN_EXCEPTION', message: 'Unknown Exception'});
            return;
        }

        response
            .status(500)
            .json({
                statusCode: 500,
                message: exception.message
            })
    }
    
}