import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class SagaState {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        name: 'state'
    })
    state: string;

    @Column({
        type: 'jsonb',
        name: 'data'
    })
    data: any;

    @Column({
        type: 'varchar',
        name: 'description',
        nullable: true
    })
    description: string;
}